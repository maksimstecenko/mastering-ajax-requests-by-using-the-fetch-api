import {
  appInputFormJoinOurProgram,
  appInputFormInputEmailJoinOurProgram,
  createSectionJoinOurProgram,
} from './join-us-section.js';

/*import { validate } from './email-validator.js';*/
                     
/*const htmlBody = document.getElementById("app-container")*/

document.addEventListener('DOMContentLoaded', createSectionJoinOurProgram());

appInputFormJoinOurProgram.addEventListener('submit', (event) => {
  event.preventDefault();
  const emailValue = appInputFormInputEmailJoinOurProgram.value;

  console.log(emailValue);

  console.log(`Entered email: ${emailValue}`);
});

/*const emailInput = document.querySelector('.app-form-input-email_join-our-program');
const submitButton = document.querySelector('.app-form-input-submit_join-our-program');

/*submitButton.addEventListener('click', () => {
  const email = emailInput.value;
  if (validate(email)) {
    alert('Email is valid');
  } else {
    alert('Wrong email');
  }
});*/

/*emailInput.addEventListener('input', (event) => {
  const email = event.target.value;
  localStorage.setItem('subscriptionEmail', email);
});

window.addEventListener('load', () => {
  const email = localStorage.getItem('subscriptionEmail');
  if (email) {
    emailInput.value = email;
  }
});

submitButton.addEventListener('click', (event) => {
  event.preventDefault();
  const email = emailInput.value;
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (emailRegex.test(email)) {
    emailInput.style.display = 'none';
    submitButton.style.textAlign = 'center';
    submitButton.textContent = 'Unsubscribe';
    localStorage.setItem('subscribed', true);
  }
 if (localStorage.getItem('subscribed')) {
    emailInput.style.display = 'block';
    emailInput.value = '';
    submitButton.style.textAlign = 'left';
    submitButton.textContent = 'Subscribe';
    localStorage.removeItem('subscribed');
    localStorage.removeItem('subscriptionEmail');
  }
});*/

document.addEventListener("DOMContentLoaded", function () {
  const emailInput = document.getElementById("email-input");
  const subscribeButton = document.getElementById("subscribe-button");
  const subscriptionForm = document.getElementById("subscription-form");

  // Populate email input with value from localStorage on page load
  const storedEmail = localStorage.getItem("subscriptionEmail");
  if (storedEmail) {
    emailInput.value = storedEmail;
  }

  // Save the value of the subscription email input to localStorage when it changes
  emailInput.addEventListener("input", function (event) {
    const email = event.target.value;
    localStorage.setItem("subscriptionEmail", email);
  });

  // Handle form submission
  subscriptionForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const email = emailInput.value;

    // If email is valid, hide the email input and update the button text
    if (validateEmail(email)) {
      emailInput.style.display = "none";
      subscribeButton.innerText = "Unsubscribe";
      localStorage.setItem("isSubscribed", "true");
    }
  });

  // Handle click on "Unsubscribe" button
  subscribeButton.addEventListener("click", function () {
    if (subscribeButton.innerText === "Unsubscribe") {
      emailInput.style.display = "block";
      subscribeButton.innerText = "Subscribe";
      localStorage.removeItem("subscriptionEmail");
      localStorage.removeItem("isSubscribed");
    }
  });

  // Check if user is already subscribed from previous session
  const isSubscribed = localStorage.getItem("isSubscribed");
  if (isSubscribed) {
    emailInput.style.display = "none";
    subscribeButton.innerText = "Unsubscribe";
  }
});

// Function to validate email format
function validateEmail(email) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}



    const subscribeForm = document.getElementById('subscription-form');
    const emailInput = document.getElementById('email-input');
    const subscribeButton = document.getElementById('subscribe-button');
    const unsubscribeButton = document.createElement('BUTTON');
    
    const communityInfo = document.getElementById('community-info');

    subscribeForm.addEventListener('submit', async (event) => {
      event.preventDefault();

      if (validate()) {
        const email = emailInput.value;
        subscribeButton.disabled = true;
        unsubscribeButton.disabled = true;
        subscribeButton.style.opacity = '0.5';
        unsubscribeButton.style.opacity = '0.5';

        try {
          const response = await fetch('http://localhost:3000/subscribe', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email })
          });

          if (response.status === 422) {
            const errorData = await response.json();
            const errorMessage = errorData.error;
            window.alert(errorMessage);
          } else {
            // Handle other response statuses
          }
        } catch (error) {
          console.error('Error:', error);
        } finally {
          subscribeButton.disabled = false;
          unsubscribeButton.disabled = false;
          subscribeButton.style.opacity = '1';
          unsubscribeButton.style.opacity = '1';
        }
      }
    });

    

    async function loadCommunityInfo() {
      try {
        const response = await fetch('http://localhost:3000/community');
        const communityData = await response.json();

        // Update the UI with community information
        // For example:
        communityInfo.innerHTML = `Big Community of People Like You: ${communityData.members}`;
      } catch (error) {
        console.error('Error:', error);
      }
    }

    loadCommunityInfo();

      unsubscribeButton.addEventListener('click', async () => {
      unsubscribeButton.disabled = true;
      unsubscribeButton.style.opacity = '0.5';

      try {
        const response = await fetch('http://localhost:3000/unsubscribe', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          // Include any necessary data in the request body
        });

        // Handle the response as needed
      } catch (error) {
        console.error('Error:', error);
      } finally {
        unsubscribeButton.disabled = false;
        unsubscribeButton.style.opacity = '1';
      }
    });
    import { validate } from './email-validator.js'
    /*function validate(email) {
      // Implement your validation logic here
      return true; // Return true if the form is valid
    }*/

